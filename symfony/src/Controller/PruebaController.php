<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PruebaController extends AbstractController
{
    /**
     * @Route("/prueba", name="prueba")
     */
    public function index()
    {
        return $this->json([
            'message' => 'Welcom to your new controller!',
            'path' => 'src/Controller/PruebaController.php',
        ]);
    }
}
